//
//  GeotaggingViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 23/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import RealmSwift

class GeotaggingViewController: UIViewController {
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var markerImage: UIImageView!
    @IBOutlet weak var confirmButton: UIButton!
    private let locationManager = CLLocationManager()
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 16.0
    var latitude = Double()
    var longitude = Double()
    var address = String()
    var photo = [UIImage]()
    
    var cardInfo = CardsREST()
    
    // The currently selected place.
    var selectedPlace: GMSPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
        latitude = Double(position.target.latitude)
        longitude = Double(position.target.longitude)
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            self.address = (lines.joined(separator: "\n"))
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func confirm(_ sender: Any) {
        confirmButton.isHidden = true
        cardInfo.geotag = [latitude, longitude]
        cardInfo.address = address
        createCard(cardInfo: cardInfo) {
            response in
            if (response == true) {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Выполнено", message: "Вы создали новую карточку", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "map"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension GeotaggingViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
    }
}

func writeRealm(photo: [UIImage]) -> [String] {
    var photoId = [String]()
    for item in photo {
        let realm = try! Realm()
        let currentPhoto = ImageRealm()
        currentPhoto.photo = (item.jpegData(compressionQuality: 1)! as NSData)
        currentPhoto.id = String(UInt(bitPattern: ObjectIdentifier(item)))
        photoId.append(currentPhoto.id!)
        try! realm.write {
            realm.add(currentPhoto)
        }
    }
    return photoId
}


// MARK: - GMSMapViewDelegate
extension GeotaggingViewController: GMSMapViewDelegate {
    
}
