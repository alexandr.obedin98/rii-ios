//
//  CardInfoViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 24/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit
import RealmSwift

class CardInfoViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var statusMarkerImageView: UIImageView!
    @IBOutlet weak var geotagLabel: UILabel!
    @IBOutlet weak var photoScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var typeTextView: UITextViewFixed!
    @IBOutlet weak var statusTextView: UITextViewFixed!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    var card: UserCardsREST?
    var newStatus: NewStatus?
    var indexPath: Int?
    
    var threshold: Double = 259200
    var imageStatus = [#imageLiteral(resourceName: "r"), #imageLiteral(resourceName: "y"), #imageLiteral(resourceName: "g")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLine(yPosition: pageControl.frame.maxY)
        addLine(yPosition: commentTextView.frame.maxY)
        addLine(yPosition: typeTextView.frame.maxY)
        addLine(yPosition: statusTextView.frame.maxY)
        
        if card != nil {
            geotagLabel.text = card?.cardsAddress
            statusMarkerImageView.image = statusMarker(createdAt: card!.cardsCreatedAt!, typeProblemLimitation: (card?.typeProblemLimitation)!)
            commentTextView.text = card?.cardsComment
            typeTextView.text = card?.typeProblemName
            statusTextView.text = card?.statusName
            
            if (statusTextView.text != "Работа по заявке выполнена") {
                agreeButton.isHidden = true
                repeatButton.isHidden = true
            }
            
            let createdAtDate = Formatter.iso8601.date(from: (card!.cardsCreatedAt)!)
            let pastPeriod = createdAtDate?.timeIntervalSinceNow
            let limitation = Double(card!.typeProblemLimitation!)
            let timer: TimeInterval = (limitation! + pastPeriod!)
            timerLabel.text = timer.format(using: [.day, .hour, .minute, .second])
            timerLabel.textColor = timerColor(createdAt: (card?.cardsCreatedAt)!, typeProblemLimitation: (card?.typeProblemLimitation)!)
        }
        // Do any additional setup after loading the view.
        
        let count = card?.cardsPhoto!.count ?? 0
        
        for i in 0..<count{
            let imageView = UIImageView()
            imageView.kf.setImage(with: getURL(from: (card?.cardsPhoto![i])!))
            imageView.contentMode = .scaleAspectFit
            let xPosition = self.view.frame.width * CGFloat(i)
            imageView.frame = CGRect(x: xPosition, y: 0, width: self.photoScrollView.frame.width, height: self.photoScrollView.frame.height)
            
            photoScrollView.contentSize.width = photoScrollView.frame.width * CGFloat(i + 1)
            photoScrollView.addSubview(imageView)
        }
        
        photoScrollView.delegate = self
        pageControl.numberOfPages = card?.cardsPhoto!.count ?? 0
        pageControl.currentPage = Int(photoScrollView.contentOffset.x / photoScrollView.frame.size.width)
        
    }
    
    func addLine (yPosition: CGFloat) {
        let lineView = UIView(frame: CGRect(x: 0, y: yPosition, width: contentView.frame.width, height: 0.2))
        lineView.layer.borderWidth = 0.2
        lineView.layer.borderColor = UIColor.lightGray.cgColor
        contentView.addSubview(lineView)
    }
    
    // MARK: - UIScrollView
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(photoScrollView.contentOffset.x / photoScrollView.frame.size.width)
        
    }
    
    func statusMarker(createdAt: String, typeProblemLimitation: String) -> UIImage {
        
        let createdAtDate = Formatter.iso8601.date(from: createdAt)
        let pastPeriod = createdAtDate?.timeIntervalSinceNow
        let limitation = Double(typeProblemLimitation)
        let timer: TimeInterval = (limitation! + pastPeriod!)
        
        if (timer > threshold) {
            return imageStatus[2]
        } else if (timer < 0) {
            return imageStatus[0]
        } else {
            return imageStatus[1]
        }
    }
    
    func timerColor(createdAt: String, typeProblemLimitation: String) -> UIColor {
        let createdAtDate = Formatter.iso8601.date(from: createdAt)
        let pastPeriod = createdAtDate?.timeIntervalSinceNow
        let limitation = Double(typeProblemLimitation)
        let timer: TimeInterval = (limitation! + pastPeriod!)
        
        if (timer > threshold) {
            return UIColor.green
        } else if (timer < 0) {
            return UIColor.red
        } else {
            return UIColor.yellow
        }
    }
    
    @IBAction func agreeAction(_ sender: Any) {
        newStatus = NewStatus()
        newStatus?.idStatus = String(1)
        updateStatus(id: (self.card?.cardsIdCard!)!, newStatus: newStatus!) {
            response in
            if (response == true) {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Выполнено", message: "Вы согласились с результатом", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadAll"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func repeatAction(_ sender: Any) {
        newStatus = NewStatus()
        newStatus?.idStatus = String(1)
        updateStatus(id: (self.card?.cardsIdCard!)!, newStatus: newStatus!) {
            response in
            if (response == true) {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Выполнено", message: "Заявка была отправлена повторно", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadAll"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension TimeInterval {
    func format(using units: NSCalendar.Unit) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = units
        formatter.unitsStyle = .abbreviated
        formatter.zeroFormattingBehavior = .pad
        
        return formatter.string(from: self)
    }}
