//
//  Cards.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 25/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Realm

class CardsRealm: Object {
    @objc dynamic var comment:String?
    @objc dynamic var idTypeProblem:String?
    @objc dynamic var limitation:String?
    @objc dynamic var idAuthor:String?
    let geotag = List<String>()
    let photo = List<String>()
}

class Item: Object {
    @objc dynamic var value:String?
    
    convenience init(value: String) {
        self.init()
        self.value = value
    }
}

class ImageRealm: Object {
    @objc dynamic var photo: NSData?
    @objc dynamic var id: String?
}



/*
 struct Cards {
 var photo: [UIImage]?
 var comment: String?
 var statusMarker: UIImage?
 var geotag: String?
 var type: String?
 var timer: String?
 }
 */
