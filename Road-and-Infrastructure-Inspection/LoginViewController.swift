//
//  LoginViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 08/11/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit
import AccountKit

class LoginViewController: UIViewController, AKFViewControllerDelegate {
    
    var accountKit: AccountKit!
    var accountID = String()
    
    var registered = false
    
    let dispatchGroup = DispatchGroup()
    
    @IBOutlet weak var informationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if accountKit == nil {
            self.accountKit = AccountKit(responseType: .accessToken)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (accountKit.currentAccessToken != nil) {
            dispatchGroup.enter()
            accountKit.requestAccount({ (account, error) in
                self.accountID = (account?.accountID as Any as? String)!
                checkAuthor(id: self.accountID) {
                    exists in
                    self.registered = exists
                    self.dispatchGroup.leave()
                }
            })
            
            dispatchGroup.notify(queue: .main) {
                if (self.registered == true) {
                    DispatchQueue.main.async(execute: {
                        self.performSegue(withIdentifier: "TabBarViewController", sender: self)
                    })
                } else {
                    let alertController = UIAlertController(title: "Ошибка", message: "Данный пользователь не зарегистрирован", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                    self.accountKit.logOut()
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is TabBarViewController
        {
            let tbvc = segue.destination as? TabBarViewController
            tbvc!.accountID = self.accountID
        }
    }
    
    func prepareLoginViewController(_ loginViewController: AKFViewController) {
        loginViewController.delegate = self
        //loginViewController.setAdvancedUIManager(nil)
        
        let them = Theme.default()
        them.headerBackgroundColor = UIColor.lightGray
        them.headerTextColor = UIColor.darkGray
        them.iconColor = UIColor.black
        them.inputTextColor = UIColor.black
        them.statusBarStyle = .default
        them.textColor = UIColor.black
        them.titleColor = UIColor.black
        loginViewController.setTheme(them)
    }
    
    @IBAction func loginWithPhone(_ sender: Any) {
        let inputState = UUID().uuidString
        let viewController = accountKit.viewControllerForPhoneLogin(with: nil, state: inputState) as AKFViewController
        
        viewController.isSendToFacebookEnabled = true
        self.prepareLoginViewController(viewController)
        self.present(viewController as! UIViewController, animated: true, completion: nil)
    }
    
    @IBAction func loginWithEmail(_ sender: Any) {
        let inputState = UUID().uuidString
        let viewController = accountKit.viewControllerForEmailLogin(with: nil, state: inputState) as AKFViewController
        
        self.prepareLoginViewController(viewController)
        self.present(viewController as! UIViewController, animated: true, completion: nil)
    }
    
    @IBAction func registration(_ sender: Any) {
        let registrationVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController
        
        self.navigationController?.pushViewController(registrationVC!, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
