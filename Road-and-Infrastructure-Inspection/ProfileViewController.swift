//
//  ProfileViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 31/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit
import AccountKit

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var accountKit: AccountKit!
    
    var author = AuthorREST()
    var accountID: String?
    
    var defoultImage = #imageLiteral(resourceName: "defprof2")
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var photoProfileImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var contactLbl: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAuthor), name: NSNotification.Name(rawValue: "reloadAuthor"), object: nil)
        
        let tbvc = self.tabBarController  as! TabBarViewController
        accountID = tbvc.accountID
        
        photoProfileImageView.image = defoultImage
        
        if accountKit == nil {
            self.accountKit = AccountKit(responseType: .accessToken)
        }
        
        load()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func reloadAuthor(notification: NSNotification){
        load()
    }
    
    func load() {
        getAuthor(id: accountID!) {
            author in
            self.author = author
            DispatchQueue.main.async(execute: {
                if (author.photoProfile != nil && author.photoProfile != "") {
                    self.photoProfileImageView.kf.setImage(with: getURL(from: author.photoProfile!))
                }
                if (author.family != nil && author.name != nil && author.patronymic != nil) {
                    self.fullNameLabel.text = author.family! + " " + author.name! + " " + author.patronymic!
                }
                if let phoneNumber = author.phoneNumber {
                    if (phoneNumber != "") {
                        self.contactLbl.text = "Номер телефона"
                        self.contactLabel.text = phoneNumber
                    }
                }
                else if let email = author.email {
                    if (email != "") {
                        self.contactLbl.text = "Электронная почта"
                        self.contactLabel.text = email
                    }
                }
            })
        }
    }
    
    func addLine (yPosition: CGFloat) {
        let lineView = UIView(frame: CGRect(x: 0, y: yPosition, width: self.view.frame.width, height: 1.5))
        lineView.layer.borderWidth = 2.5
        lineView.layer.borderColor = UIColor.black.cgColor
        self.view.addSubview(lineView)
    }
    
    @IBAction func logOut(_ sender: Any) {
        accountKit.logOut()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func edit(_ sender: Any) {
        let editVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileEditViewController") as? ProfileEditViewController
        editVC!.authorEdit = author
        self.navigationController?.pushViewController(editVC!, animated: true)
    }
    
    @IBAction func changePhoto(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "Добавить фото", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (action: UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                print("Камера недоступна")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        if (photoProfileImageView.image != defoultImage) {
            actionSheet.addAction(UIAlertAction(title: "Удалить фото", style: .destructive, handler: { (action: UIAlertAction) in
                self.photoProfileImageView.image = self.defoultImage
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        photoProfileImageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showMyAchievements(_ sender: Any) {
        let achievementsVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyAchievementsTableViewController") as? MyAchievementsTableViewController
        self.navigationController?.pushViewController(achievementsVC!, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
