//
//  RegistrationViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 10/11/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit
import AccountKit

class RegistrationViewController: UIViewController, AKFViewControllerDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var accountKit: AccountKit!
    var accountID = String()
    var author = AuthorREST()
    
    let dispatchGroup = DispatchGroup()
    
    var defoultImage = #imageLiteral(resourceName: "defprof2")
    var registered = false
    
    @IBOutlet weak var registrationByPhoneNumber: UIButton!
    @IBOutlet weak var registrationByEmail: UIButton!
    @IBOutlet weak var photoProfileImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var familyTextField: UITextField!
    @IBOutlet weak var patronymicTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photoProfileImageView.image = defoultImage
        
        if accountKit == nil {
            self.accountKit = AccountKit(responseType: .accessToken)
        }
        
        registrationByPhoneNumber.isEnabled = false
        registrationByEmail.isEnabled = false
        [nameTextField, familyTextField, patronymicTextField].forEach({$0.addTarget(self, action: #selector(editingChanged), for: .editingChanged) })
        
        // Do any additional setup after loading the view.
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        
        textField.text = textField.text?.trimmingCharacters(in: .whitespaces)
        
        guard
            let name = nameTextField.text, !name.isEmpty,
            let family = familyTextField.text, !family.isEmpty,
            let patronymic = patronymicTextField.text, !patronymic.isEmpty
            else {
                registrationByPhoneNumber.isEnabled = false
                registrationByEmail.isEnabled = false
                return
        }
        registrationByPhoneNumber.isEnabled = true
        registrationByEmail.isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (accountKit.currentAccessToken != nil) {
            dispatchGroup.enter()
            accountKit.requestAccount({ (account, error) in
                self.accountID = (account?.accountID as Any as? String)!
                
                self.author.name = self.nameTextField.text
                self.author.family = self.familyTextField.text
                self.author.patronymic = self.patronymicTextField.text
                self.author.photoProfile = (self.photoProfileImageView.image != self.defoultImage) ? convertImageTobase64(format: .jpeg, image: (self.photoProfileImageView.image!)) : ""
                self.author.idAuthor = account?.accountID as Any as? String
                self.author.email = account?.emailAddress ?? ""
                self.author.phoneNumber = account?.phoneNumber?.stringRepresentation() ?? ""
                
                createAuthor(author: self.author) {response in
                    self.registered = response
                    self.dispatchGroup.leave()
                }
            })
            
            dispatchGroup.notify(queue: .main) {
                if (self.registered == true) {
                    DispatchQueue.main.async(execute: {
                        let alertController = UIAlertController(title: "Регистрация выполнена", message: "Вы зарегистрированили данные учетной записи", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popViewController(animated: true)
                            self.performSegue(withIdentifier: "TabBarViewController", sender: self)
                        }))
                        self.present(alertController, animated: true, completion: nil)
                    })
                } else {
                    let alertController = UIAlertController(title: "Ошибка", message: "Данный пользователь уже зарегистрирован", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                    self.accountKit.logOut()
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is TabBarViewController
        {
            let tbvc = segue.destination as? TabBarViewController
            tbvc!.accountID = self.accountID
        }
    }
    
    func prepareRegistrationViewController(_ registrationViewController: AKFViewController) {
        registrationViewController.delegate = self
        //registrationViewController.setAdvancedUIManager(nil)
        
        let them = Theme.default()
        them.headerBackgroundColor = UIColor.lightGray
        them.headerTextColor = UIColor.darkGray
        them.iconColor = UIColor.black
        them.inputTextColor = UIColor.black
        them.statusBarStyle = .default
        them.textColor = UIColor.black
        them.titleColor = UIColor.black
        registrationViewController.setTheme(them)
    }
    
    @IBAction func registerByEmail(_ sender: Any) {
        let inputState = UUID().uuidString
        let viewController = accountKit.viewControllerForEmailLogin(with: nil, state: inputState) as AKFViewController
        
        self.prepareRegistrationViewController(viewController)
        self.present(viewController as! UIViewController, animated: true, completion: nil)
    }
    
    @IBAction func registerByPhone(_ sender: Any) {
        let inputState = UUID().uuidString
        let viewController = accountKit.viewControllerForPhoneLogin(with: nil, state: inputState) as AKFViewController
        
        viewController.isSendToFacebookEnabled = true
        self.prepareRegistrationViewController(viewController)
        self.present(viewController as! UIViewController, animated: true, completion: nil)
    }
    
    @IBAction func addProfilePhoto(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "Добавить фото", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (action: UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                print("Камера недоступна")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        if (photoProfileImageView.image != defoultImage) {
            actionSheet.addAction(UIAlertAction(title: "Удалить фото", style: .destructive, handler: { (action: UIAlertAction) in
                self.photoProfileImageView.image = self.defoultImage
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        photoProfileImageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func removeKeyboardTap(_ sender: Any) {
        familyTextField.resignFirstResponder()
        nameTextField.resignFirstResponder()
        patronymicTextField.resignFirstResponder()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
