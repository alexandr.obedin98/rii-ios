//
//  CardsREST.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 03/11/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import Foundation
import UIKit

struct DBResponseType: Codable {
    enum CodingKeys: String, CodingKey {
        case data
        case status
        case message
    }
    let data: [TypeProblemREST]
    var status: String?
    var message: String?
}

struct TypeProblemREST: Codable {
    var name: String?
    var idTypeProblem: String?
    var limitation: String?
    enum CodingKeys: String, CodingKey {
        case name
        case idTypeProblem = "id_type_problem"
        case limitation
    }
}

struct DBResponseCheck: Codable {
    enum CodingKeys: String, CodingKey {
        case data
    }
    let data: exists
}

struct exists: Codable {
    var exists: Bool?
    enum CodingKeys: String, CodingKey {
        case exists
    }
}

struct DBResponseAuthor: Codable {
    enum CodingKeys: String, CodingKey {
        case data
    }
    let data: AuthorREST
}

struct AuthorREST: Codable {
    var photoProfile:String?
    var phoneNumber:String?
    var email:String?
    var idAuthor:String?
    var name:String?
    var family:String?
    var patronymic:String?
    
    enum CodingKeys: String, CodingKey {
        case photoProfile = "photo_profile"
        case phoneNumber = "phone_number"
        case email
        case idAuthor = "id_author"
        case name
        case family
        case patronymic
    }
}

struct CardsREST: Codable {
    var photo:[String]?
    var comment:String?
    var idTypeProblem:String?
    var idAuthor:String?
    var geotag:[Double]?
    var address:String?
    
    enum CodingKeys: String, CodingKey {
        case photo
        case comment
        case idTypeProblem = "id_type_problem"
        case idAuthor = "id_author"
        case geotag
        case address
    }
    
    /*func encode(to encoder: Encoder) throws {
     var container = encoder.container(keyedBy: CodingKeys.self)
     try container.encode(photo, forKey: .photo)
     try container.encode(comment, forKey: .comment)
     try container.encode(idTypeProblem, forKey: .idTypeProblem)
     try container.encode(idAuthor, forKey: .idAuthor)
     try container.encode(geotag, forKey: .geotag)
     try container.encode(address, forKey: .address)
     }
     init(from decoder: Decoder) throws {
     let container = try decoder.container(keyedBy: CodingKeys.self)
     photo = try container.decode([String].self, forKey: .photo)
     comment = try container.decode(String.self, forKey: .comment)
     idTypeProblem = try container.decode(Int64.self, forKey: .idTypeProblem)
     idAuthor = try container.decode(Int64.self, forKey: .idAuthor)
     geotag = try container.decode([Double].self, forKey: .geotag)
     address = try container.decode(String.self, forKey: .address)
     }*/
}

struct DBResponseMap: Codable {
    enum CodingKeys: String, CodingKey {
        case data
        case status
        case message
    }
    let data: [CardsForMapREST]
    var status: String?
    var message: String?
}

struct CardsForMapREST: Codable {
    var typeProblemName:String?
    var cardsGeotag:[Double]?
    var cardsAddress:String?
    var cardsCreatedAt: String?
    var typeProblemLimitation: String?
    
    enum CodingKeys: String, CodingKey {
        case typeProblemName = "typeproblemname"
        case cardsGeotag = "cardsgeotag"
        case cardsAddress = "cardsaddress"
        case cardsCreatedAt = "cardscreatedat"
        case typeProblemLimitation = "typeproblemlimitation"
    }
}

struct DBResponse: Codable {
    enum CodingKeys: String, CodingKey {
        case data
        case status
        case message
    }
    let data: [UserCardsREST]
    var status: String?
    var message: String?
}

struct UserCardsREST: Codable {
    var cardsIdCard: String?
    var cardsComment: String?
    var cardsPhoto: [String]?
    var typeProblemName:String?
    var cardsGeotag:[Double]?
    var cardsAddress:String?
    var cardsCreatedAt: String?
    var typeProblemLimitation: String?
    var statusName: String?
    
    enum CodingKeys: String, CodingKey {
        case cardsIdCard = "cardsidcard"
        case cardsComment = "cardscomment"
        case cardsPhoto = "cardsphoto"
        case typeProblemName = "typeproblemname"
        case cardsGeotag = "cardsgeotag"
        case cardsAddress = "cardsaddress"
        case cardsCreatedAt = "cardscreatedat"
        case typeProblemLimitation = "typeproblemlimitation"
        case statusName = "statusname"
    }
}

struct TypeProblem: Codable {
    var idTypeProblem: String?
    var name: String?
    var limitation: String?
    
    
    enum CodingKeys: String, CodingKey {
        case idTypeProblem = "id_type_problem"
        case name
        case limitation
    }
}

struct NewStatus: Codable {
    var idStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case idStatus = "idStatus"
    }
}
