//
//  ProfileEditViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 15/11/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit

class ProfileEditViewController: UIViewController {
    
    var authorEdit: AuthorREST?
    @IBOutlet weak var accountIdLabel: UILabel!
    @IBOutlet weak var familyTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var patronymicTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountIdLabel.text = authorEdit!.idAuthor
        familyTextField.text = authorEdit?.family
        nameTextField.text = authorEdit?.name
        patronymicTextField.text = authorEdit?.patronymic
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     
     */
    @IBAction func edit(_ sender: Any) {
        authorEdit!.name = nameTextField.text
        authorEdit!.family = familyTextField.text
        authorEdit!.patronymic = patronymicTextField.text
            updateAuthor(id: (self.authorEdit?.idAuthor)!, author: self.authorEdit!) {
                response in
                if (response == true) {
                    DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Выполнено", message: "Вы изменили данные учетной карточки", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAuthor"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func removeKeyboardTap(_ sender: Any) {
        familyTextField.resignFirstResponder()
        nameTextField.resignFirstResponder()
        patronymicTextField.resignFirstResponder()
    }
    
}
