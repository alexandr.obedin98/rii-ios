//
//  CardsTableViewCell.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 25/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit

class CardsTableViewCell: UITableViewCell {

    @IBOutlet weak var statusMarkerImageView: UIImageView!
    @IBOutlet weak var geotagLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
