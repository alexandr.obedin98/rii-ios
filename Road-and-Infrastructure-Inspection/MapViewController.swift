//
//  MapViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 24/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit
import GoogleMaps
import RealmSwift

class MapViewController: UIViewController {
    
    @IBOutlet var mapView: GMSMapView!
    private let locationManager = CLLocationManager()
    var zoomLevel: Float = 12.0
    var latitude = Double()
    var longitude = Double()
    
    var threshold: Double = 259200
    var imageStatus = [#imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "yellow"), #imageLiteral(resourceName: "green")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name(rawValue: "map"), object: nil)
                
        mapView.delegate = self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        reload()

        locationManager.startUpdatingLocation()
        // Do any additional setup after loading the view.
    }
    
    @objc func reload(){
        mapView.clear()
        getCardsForMap() {
            cards in
            for card in cards {
                DispatchQueue.main.async {
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude: card.cardsGeotag![0], longitude: card.cardsGeotag![1])
                    marker.title = card.typeProblemName
                    
                    let createdAtDate = Formatter.iso8601.date(from: (card.cardsCreatedAt)!)
                    let pastPeriod = createdAtDate?.timeIntervalSinceNow
                    let limitation = Double(card.typeProblemLimitation!)
                    let timer: TimeInterval = (limitation! + pastPeriod!)
                    marker.icon = self.statusMarker(timer: timer)
                    
                    marker.snippet = timer.format(using: [.day, .hour, .minute, .second])
                    marker.map = self.mapView
                }
            }
        }
    }
    
    func statusMarker(timer: TimeInterval) -> UIImage {
        
        if (timer > threshold) {
            return imageStatus[2]
        } else if (timer < 0) {
            return imageStatus[0]
        } else {
            return imageStatus[1]
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// MARK: - CLLocationManagerDelegate
//1
extension MapViewController: CLLocationManagerDelegate {
    // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        // 7
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 13, bearing: 0, viewingAngle: 0)
        
        // 8
        locationManager.stopUpdatingLocation()
    }
}

// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    
}
