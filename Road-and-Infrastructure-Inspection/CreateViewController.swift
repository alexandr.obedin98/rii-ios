//
//  CreateViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 16/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit

class CreateViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var dataSource = [TypeProblemREST]()
    var imageArray = [UIImage]()
    var imageAdd = #imageLiteral(resourceName: "add")
    var imageCrowded = #imageLiteral(resourceName: "r")
    var imageRemove = #imageLiteral(resourceName: "delete")
    var indexAdd = 0
    var tapIndex = 0
    var longPressIndex = 0
    var crowdedFlag = false
    
    var pointBegan = CGFloat()
    
    var typeProblem = String()
    var author: String?
    
    var createCard = CardsREST()
    
    var swipeGesture  = UISwipeGestureRecognizer()
    
    @IBOutlet weak var PhotoScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var typePickerView: UIPickerView!
    @IBOutlet weak var markPlaceButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name(rawValue: "load"), object: nil)
        
        let tbvc = self.tabBarController  as! TabBarViewController
        author = tbvc.accountID
        
        markPlaceButton.isEnabled = false
        
        PhotoScrollView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture))
        PhotoScrollView.addGestureRecognizer(tapGesture)
        
        typePickerView.dataSource = self
        typePickerView.delegate = self
        
        commentTextView.text = "Добавить комментарий"
        commentTextView.textColor = UIColor.lightGray
        commentTextView.font = UIFont(name: "verdana", size: 16.0)
        commentTextView.returnKeyType = .done
        commentTextView.delegate = self
        
        imageArray = [imageAdd]
        
        PhotoScrollView.delegate = self
        
        pageControl.numberOfPages = 0
        
        addLine(yPosition: pageControl.frame.maxY)
        addLine(yPosition: commentTextView.frame.maxY)
        
        clearView()
        drowScrollView()
        
        getTypeProblem() {
            types in
            DispatchQueue.main.async {
                self.dataSource = types
                self.typePickerView.reloadAllComponents()
            }
        }
    }
    
    func clearView() {
        let subViews = self.PhotoScrollView.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
    }
    
    func drowScrollView() {
        for i in 0..<imageArray.count {
            let imageView = UIImageView(image: imageArray[i])
            imageView.contentMode = .scaleAspectFit
            let xPosition = self.view.frame.width * CGFloat(i)
            imageView.frame = CGRect(x: xPosition, y: 0, width: self.PhotoScrollView.frame.width, height: self.PhotoScrollView.frame.height)
            
            PhotoScrollView.contentSize.width = PhotoScrollView.frame.width * CGFloat(i + 1)
            PhotoScrollView.addSubview(imageView)
        }
    }
    
    @objc func reload(){
        indexAdd = 0
        let subViews = self.PhotoScrollView.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        typePickerView.selectRow(0, inComponent: 0, animated: true)
        viewDidLoad()
    }
    
    func addLine (yPosition: CGFloat) {
        let lineView = UIView(frame: CGRect(x: 0, y: yPosition, width: self.view.frame.width, height: 0.2))
        lineView.layer.borderWidth = 0.2
        lineView.layer.borderColor = UIColor.lightGray.cgColor
        self.view.addSubview(lineView)
    }
    
    @IBAction func removeKeyboardTap(_ sender: Any) {
        commentTextView.resignFirstResponder()
    }
    
    // MARK: - UITextViewDelegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Добавить комментарий"  {
            textView.text = ""
            textView.textColor = UIColor.black
            textView.font = UIFont(name: "verdana", size: 18.0)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Добавить комментарий"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont(name: "verdana", size: 16.0)
        }
    }
    
    // MARK: - UIImageView
    @objc func tapGesture() {
        tapIndex = (Int)(PhotoScrollView.contentOffset.x / PhotoScrollView.frame.size.width)
        if (tapIndex == indexAdd && !crowdedFlag) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            let actionSheet = UIAlertController(title: "Добавить фото", message: "", preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (action: UIAlertAction) in
                
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    imagePickerController.sourceType = .camera
                    self.present(imagePickerController, animated: true, completion: nil)
                } else {
                    print("Камера недоступна")
                }
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { (action: UIAlertAction) in
                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    func removeBlur() {
        let blurImage = (PhotoScrollView.subviews)[longPressIndex]
        for subview in blurImage.subviews {
            subview.removeFromSuperview()
        }
    }
    
    // MARK: - UIScrollView
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = Int(PhotoScrollView.contentOffset.x / PhotoScrollView.frame.size.width)
        if (index != indexAdd && index <= imageArray.count) {
            pageControl.numberOfPages = (imageArray.count - 1)
            pageControl.currentPage = index - 1
        } else {
            pageControl.numberOfPages = 0
        }
    }
    
    // MARK: - UIImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imageArray.insert(image, at: 1)
        
        if  imageArray.count == 5 {
            imageArray.remove(at: 0)
            imageArray.insert(imageCrowded, at: 0)
            crowdedFlag = true
        }
        
        clearView()
        drowScrollView()
        
        picker.dismiss(animated: true, completion: nil)
        pageControl.numberOfPages = (imageArray.count - 1)
        
        PhotoScrollView.setContentOffset(CGPoint(x: PhotoScrollView.frame.size.width, y: 0), animated: true)
        
        imageArray.count > 1 ? (markPlaceButton.isEnabled = true) : (markPlaceButton.isEnabled = false)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func removeImageLongPress(_ sender: UILongPressGestureRecognizer) {
        if (Int)(PhotoScrollView.contentOffset.x / PhotoScrollView.frame.size.width) != indexAdd {
            if sender.state == UIGestureRecognizer.State.began {
                pointBegan = sender.location(in: PhotoScrollView).y
                
                longPressIndex = (Int)(PhotoScrollView.contentOffset.x / PhotoScrollView.frame.size.width)
                let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .prominent))
                let blurImage = (PhotoScrollView.subviews)[longPressIndex]
                visualEffectView.frame = blurImage.bounds
                blurImage.addSubview(visualEffectView)
                let removeImageView = UIImageView(image:imageRemove)
                removeImageView.contentMode = .scaleAspectFit
                removeImageView.frame = blurImage.bounds
                blurImage.addSubview(removeImageView)
            }
            
            if sender.state == UIGestureRecognizer.State.ended {
                if pointBegan > sender.location(in: PhotoScrollView).y + 50 {
                    removeImage()
                } else {
                    removeBlur()
                }
            }
        }
    }
    
    func removeImage() {
        imageArray.remove(at: longPressIndex)
        imageArray.remove(at: 0)
        imageArray.insert(imageAdd, at: 0)
        
        clearView()
        drowScrollView()
        
        crowdedFlag = false
        
        pageControl.numberOfPages = (imageArray.count - 1)
        pageControl.currentPage = Int(PhotoScrollView.contentOffset.x / PhotoScrollView.frame.size.width)
        
        if imageArray.count > 1 {
            markPlaceButton.isEnabled = true
            PhotoScrollView.setContentOffset(CGPoint(x: PhotoScrollView.frame.size.width, y: 0), animated: true)
        } else {
            markPlaceButton.isEnabled = false
            
        }
    }
    
    
    @IBAction func markPlace(_ sender: Any) {
        var base64Array = [String]()
        var photo = imageArray
        photo.remove(at: 0)
        for pht in photo {
            base64Array.append(convertImageTobase64(format: .jpeg, image: pht)!)
        }
        
        if commentTextView.text == "Добавить комментарий"  {
            commentTextView.text = "Без комментариев"
        }
        
        createCard = CardsREST(photo: base64Array, comment: commentTextView.text,
                               idTypeProblem: dataSource[typePickerView.selectedRow(inComponent: 0)].idTypeProblem!,
                               idAuthor: author, geotag: [], address: "")
        
        let gcv = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GeotaggingViewController") as? GeotaggingViewController
        gcv!.cardInfo = createCard
        gcv!.photo = photo
        self.navigationController?.pushViewController(gcv!, animated: true)
    }
}

// MARK: - UIPickerViewDelegates
extension CreateViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var dataSourceRow = [String]()
        for item in dataSource {
            dataSourceRow.append(item.name!)
        }
        return dataSourceRow[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    }
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


