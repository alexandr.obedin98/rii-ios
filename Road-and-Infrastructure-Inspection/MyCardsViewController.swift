//
//  MyCardsViewController.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 24/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import UIKit
import RealmSwift
import Kingfisher

class MyCardsViewController: UITableViewController {
    
    @IBOutlet var CardsTableView: UITableView!
    
    var refreshCtrl = UIRefreshControl()
    
    var imageArray = [UIImage]()
    var cardsArray = [UserCardsREST]()
    
    var threshold: Double = 259200
    var imageStatus = [#imageLiteral(resourceName: "r"), #imageLiteral(resourceName: "y"), #imageLiteral(resourceName: "g")]
    
    var author: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(loadCard), name: NSNotification.Name(rawValue: "load"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadTable), name: NSNotification.Name(rawValue: "loadAll"), object: nil)
        let tbvc = self.tabBarController  as! TabBarViewController
        author = tbvc.accountID
        
        CardsTableView.addSubview(refreshCtrl)
        refreshCtrl.attributedTitle = NSAttributedString(string: "Обновление")
        refreshCtrl.tintColor = UIColor.blue
        refreshCtrl.addTarget(self, action: #selector(loadTable), for: .valueChanged)
        
        loadTable()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cardsArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath) as! CardsTableViewCell
        let card = cardsArray[indexPath.row]
        cell.geotagLabel.text = card.cardsAddress
        
        cell.photoImageView.kf.setImage(with: getURL(from: card.cardsPhoto![0]))
        cell.statusMarkerImageView.image = statusMarker(createdAt: card.cardsCreatedAt!, typeProblemLimitation: card.typeProblemLimitation!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let card = cardsArray[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cardInfoViewController = storyboard.instantiateViewController(withIdentifier: "CardInfoViewController") as!
        CardInfoViewController
        cardInfoViewController.card = card
        cardInfoViewController.indexPath = indexPath.row
        self.navigationController?.pushViewController(cardInfoViewController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentImage = cardsArray[indexPath.row]
        let imageView = UIImageView()
        imageView.kf.setImage(with: getURL(from: (currentImage.cardsPhoto![0])))
        let imageRatio = imageView.image?.getImageRatio() ?? getImage(from: currentImage.cardsPhoto![0]).getImageRatio()
        return tableView.frame.width / imageRatio
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
    @objc func loadCard(notification: NSNotification){
        getLastCard(id: author!) {
            cards in
            self.cardsArray.insert(cards[0], at: 0)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func loadTable() {
        getUserCards(id: author!) {
            cards in
            self.cardsArray.removeAll()
            self.cardsArray = cards
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.refreshCtrl.endRefreshing()
            }
        }
    }
    
    func statusMarker(createdAt: String, typeProblemLimitation: String) -> UIImage {
        let createdAtDate = Formatter.iso8601.date(from: createdAt)
        let pastPeriod = createdAtDate?.timeIntervalSinceNow
        let limitation = Double(typeProblemLimitation)
        let timer: TimeInterval = (limitation! + pastPeriod!)
        
        if (timer > threshold) {
            return imageStatus[2]
        } else if (timer < 0) {
            return imageStatus[0]
        } else {
            return imageStatus[1]
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UIImage {
    func getImageRatio() -> CGFloat {
        let imageRatio = CGFloat(self.size.width / (self.size.height))
        return imageRatio
    }
}

extension Formatter {
    static let iso8601: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return formatter
    }()
}
