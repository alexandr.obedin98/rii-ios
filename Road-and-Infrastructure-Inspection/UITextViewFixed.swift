//
//  UITextViewFixed.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 17/05/2019.
//  Copyright © 2019 Александр Обедин. All rights reserved.
//

import UIKit

@IBDesignable class UITextViewFixed: UITextView {
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
}
