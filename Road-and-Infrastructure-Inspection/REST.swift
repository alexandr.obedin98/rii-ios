//
//  REST.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 31/10/2018.
//  Copyright © 2018 Александр Обедин. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Kingfisher

var baseApiURL = "http://rii.rd-science.com"
//var baseApiURL = "http://localhost:3000"

func getTypeProblem(completionHandler: @escaping (_ types: [TypeProblemREST]) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/type")
        else {
            return
    }
    let session = URLSession.shared
    session.dataTask(with: url) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        
        guard let responseData = data else {
            print("Error: did not receive data")
            return
        }
        
        // parse the result as JSON, since that's what the API provides
        do {
            let parsedResult = try JSONDecoder().decode(DBResponseType.self, from: responseData)
            completionHandler(parsedResult.data)
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
        }.resume()
}

func getCardsForMap(completionHandler: @escaping (_ cards: [CardsForMapREST]) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/cards")
        else {
            return
    }
    let session = URLSession.shared
    session.dataTask(with: url) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        
        guard let responseData = data else {
            print("Error: did not receive data")
            return
        }
        
        // parse the result as JSON, since that's what the API provides
        do {
            let parsedResult = try JSONDecoder().decode(DBResponseMap.self, from: responseData)
            completionHandler(parsedResult.data)
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
        }.resume()
}

func getUserCards(id: String, completionHandler: @escaping (_ cards: [UserCardsREST]) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/cards/" + id)
        else {
            return
    }
    let session = URLSession.shared
    session.dataTask(with: url) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        
        guard let responseData = data else {
            print("Error: did not receive data")
            return
        }
        
        // parse the result as JSON, since that's what the API provides
        do {
            let parsedResult = try JSONDecoder().decode(DBResponse.self, from: responseData)
            completionHandler(parsedResult.data)
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
        }.resume()
}

func getLastCard(id: String, completionHandler: @escaping (_ cards: [UserCardsREST]) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/last/" + id)
        else {
            return
    }
    let session = URLSession.shared
    session.dataTask(with: url) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        
        guard let responseData = data else {
            print("Error: did not receive data")
            return
        }
        
        // parse the result as JSON, since that's what the API provides
        do {
            let parsedResult = try JSONDecoder().decode(DBResponse.self, from: responseData)
            completionHandler(parsedResult.data)
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
        }.resume()
}

func createCard(cardInfo: CardsREST, completionHandler: @escaping (_ response: Bool) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/cards")
        else {
            return
    }
    guard let encodedData = try? JSONEncoder().encode(cardInfo)
        else {
            return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpBody = encodedData
    let session = URLSession.shared
    session.dataTask(with: request) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        guard let data = data
            else {
                return
        }
        print(data)
        do {
            try JSONSerialization.jsonObject(with: data, options: [])
            completionHandler(true)
        } catch {
            completionHandler(false)
        }
        }.resume()
}

func createAuthor(author: AuthorREST, completionHandler: @escaping (_ response: Bool) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/author")
        else {
            return
    }
    guard let encodedData = try? JSONEncoder().encode(author)
        else {
            return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpBody = encodedData
    let session = URLSession.shared
    session.dataTask(with: request) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        guard let data = data
            else {
                return
        }
        print(data)
        do {
            try JSONSerialization.jsonObject(with: data, options: [])
            completionHandler(true)
        } catch {
            completionHandler(false)
        }
        }.resume()
}

func checkAuthor(id: String, completionHandler: @escaping (_ exists: Bool) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/check/" + id)
        else {
            return
    }
    let session = URLSession.shared
    session.dataTask(with: url) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        
        guard let responseData = data else {
            print("Error: did not receive data")
            return
        }
        
        // parse the result as JSON, since that's what the API provides
        do {
            let parsedResult = try JSONDecoder().decode(DBResponseCheck.self, from: responseData)
            completionHandler(parsedResult.data.exists!)
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
        }.resume()
}

func getAuthor(id: String, completionHandler: @escaping (_ author: AuthorREST) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/author/" + id)
        else {
            return
    }
    let session = URLSession.shared
    session.dataTask(with: url) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        
        guard let responseData = data else {
            print("Error: did not receive data")
            return
        }
        
        // parse the result as JSON, since that's what the API provides
        do {
            let parsedResult = try JSONDecoder().decode(DBResponseAuthor.self, from: responseData)
            completionHandler(parsedResult.data)
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
        }.resume()
}

func updateAuthor(id: String, author: AuthorREST, completionHandler: @escaping (_ response: Bool) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/author/" + id)
        else {
            return
    }
    guard let encodedData = try? JSONEncoder().encode(author)
        else {
            return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "PUT"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpBody = encodedData
    let session = URLSession.shared
    session.dataTask(with: request) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        guard let data = data
            else {
                return
        }
        print(data)
        do {
            try JSONSerialization.jsonObject(with: data, options: [])
            completionHandler(true)
        } catch {
            completionHandler(false)
        }
        }.resume()
}

func updateStatus(id: String, newStatus: NewStatus, completionHandler: @escaping (_ response: Bool) -> ()) {
    guard let url = URL(string: baseApiURL + "/api/status/" + id)
        else {
            return
    }
    guard let encodedData = try? JSONEncoder().encode(newStatus)
        else {
            return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "PUT"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpBody = encodedData
    let session = URLSession.shared
    session.dataTask(with: request) {
        (data, response, error) in
        if let response = response {
            print(response)
        }
        guard let data = data
            else {
                return
        }
        print(data)
        do {
            try JSONSerialization.jsonObject(with: data, options: [])
            completionHandler(true)
        } catch {
            completionHandler(false)
        }
        }.resume()
}

public enum ImageFormat {
    case png
    case jpeg
}

func getURL(from URLstr: String) -> URL {
    print(baseApiURL + "/attachment/" + URLstr + ".jpg")
    return URL(string: baseApiURL + "/attachment/" + URLstr + ".jpg")!
}

func getImage(from URLstr: String) -> UIImage {
    let data = try? Data(contentsOf: getURL(from: URLstr))
    return UIImage(data: data!)!
}

func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
    var imageData: Data?
    switch format {
    case .png: imageData = image.pngData()
    case .jpeg: imageData = image.jpegData(compressionQuality: 0.5)
    }
    return imageData?.base64EncodedString()
}

