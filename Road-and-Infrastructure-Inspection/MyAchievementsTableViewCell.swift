//
//  MyAchievementsTableViewCell.swift
//  Road-and-Infrastructure-Inspection
//
//  Created by Александр Обедин on 19/04/2019.
//  Copyright © 2019 Александр Обедин. All rights reserved.
//

import UIKit

class MyAchievementsTableViewCell: UITableViewCell {

    @IBOutlet weak var achievementImageView: UIImageView!
    @IBOutlet weak var achievementLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
